﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip enemyFireSound;
    [SerializeField] float enemyFireSoundVolume = 0.1f;
    [SerializeField] AudioClip enemyDied;
    [SerializeField] float enemyDiedSoundVolume = 0.1f;

    private int health = 100;
    private int damage;

    // Start is called before the first frame update
    void Start()
    {
        damage = Random.Range(10, 50);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            health -= Bullet.damageB;

            if (health <= 0)
            {
                AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
                Destroy(this.gameObject);
            }
        }
    }
}
