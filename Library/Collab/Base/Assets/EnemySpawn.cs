﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject Enemy;

    float maxSpawn = 5f;

    void Start()
    {
        Invoke("SpawnEnemy", maxSpawn);

        InvokeRepeating("IncreaseSpawnRate", 0f, f);
    }


    void Update()
    {

    }

    void SpawnEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject aiEnemy = (GameObject)Instantiate(Enemy);
        aiEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        NextEnemySpawn();
    }

    void NextEnemySpawn()
    {
        float spawnInSeconds;
        if (maxSpawn > 1f)
        {
            spawnInSeconds = Random.Range(1f, maxSpawn);
        }
        else
        {
            spawnInSeconds = 1f;
        }
        Invoke("SpawnEnemy", spawnInSeconds);
    }

    void IncreaseSpawnRate()
    {
        if (maxSpawn > 1f)
        {
            maxSpawn--;
        }
        if (maxSpawn == 1f)
        {
            CancelInvoke("IncreaseSpawnRate");
        }
    }
}
