﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip enemyFireSound;
    [SerializeField] float enemyFireSoundVolume = 0.1f;
    [SerializeField] AudioClip enemyDied;
    [SerializeField] float enemyDiedSoundVolume = 0.1f;

    private int health = 100;
    private int damage;

    public float speed =3f;
    Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        damage = Random.Range(10, 40);
        rb2d.velocity = new Vector3(0, -2);
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMove();
    }
    private void EnemyMove()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            health -= Bullet.damageB;

            if (health <= 0)
            {
                AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
                Destroy(this.gameObject);
            }
        }
    }
}
