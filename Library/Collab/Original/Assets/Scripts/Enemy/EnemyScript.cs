﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerShip;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip enemyFireSound;
    [SerializeField] float enemyFireSoundVolume = 0.1f;
    [SerializeField] AudioClip enemyDied;
    [SerializeField] float enemyDiedSoundVolume = 0.1f;

    private int health = 100;
    private int score = 10;
    public static int damage;

    public float speed = 3f;
    

    // Start is called before the first frame update
    void Start()
    {
        damage = Random.Range(10, 40);
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMove();
    }
    private void EnemyMove()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            health -= Bullet.damageB;
            if (health <= 0)
            {
                score += PlayerController.playerScore;

                Destroy(this.gameObject, 3f);
            }

            AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
        }

        if (collision.gameObject.tag == "DestroyWall")
        {
            Destroy(collision.gameObject);
        }
    }
}
