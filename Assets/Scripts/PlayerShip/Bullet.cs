﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;

    public static int DamageNormalBullet = 20;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.up * speed;    
    }

    private void Update()
    {
        if (Time.timeScale == 0)
        {
            Destroy(this.gameObject);
        }
    }
    // Update is called once per frame

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag ==  "Enemy")
        {
            Destroy(this.gameObject);
        }

        if(col.gameObject.tag == "DestroyWall")
        {
            Destroy(this.gameObject);
        }
        if(col.gameObject.tag == "BossEnemy")
        {
            Destroy(this.gameObject);
        }
        if(col.gameObject.tag == "BossBullet")
        {
            Destroy(this.gameObject);
        }
    }
}
