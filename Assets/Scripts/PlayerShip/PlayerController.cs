﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using TMPro;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] AudioClip playerFireSound;
        [SerializeField] float playerFireSoundVolume = 0.1f;
        [SerializeField] AudioClip playerDied;
        [SerializeField] float playerDiedSoundVolume = 0.1f;
        [SerializeField] private float timeSpeed = 5f;
        [SerializeField] private float playerShipSpeed = 10;

        private Vector2 movementInput = Vector2.zero;


        public GameObject bulletPrefab;
        public Transform firePoint;

        public static int health;
        public static int playerScore;
        public static bool isDead;

        // Start is called before the first frame update
        void Start()
        {
            isDead = false;
            Application.targetFrameRate = -1;

            health = 100;
            playerScore = 0;
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }

        private void Move()
        {
            movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));  
            var newX = transform.position.x + movementInput.x*Time.deltaTime*playerShipSpeed;
            var newY = transform.position.y + movementInput.y*Time.deltaTime*playerShipSpeed;
            transform.position = new Vector2(newX, newY);

            if (Input.GetKeyDown(KeyCode.Space))
            { 
                Shoot();
            }
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }

        void Shoot()
        {
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Enemy")
            {
                health -= EnemyScript.damage;
                if (health <= 0)
                {
                    AudioSource.PlayClipAtPoint(playerDied, Camera.main.transform.position, playerDiedSoundVolume);
                    Destroy(this.gameObject);
                }
            }

            if(other.gameObject.tag == "BossBullet")
            {
                health -= BossBullet.DamageNormalBullet;
            }
        }
        
    }
}
