﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Manager
{
    public class SoundManager : Singleton<SoundManager>
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        public static SoundManager Instance { get; private set; }

        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }

        public enum Sound
        {
            BGM,
            Fire,
            EnemyFire,
            Explosion,
        }

        public void Play(AudioSource audioSource, Sound sound)
        {

            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }

        //Play BGM
        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM);

        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }

            Debug.Assert(false, $"cannot find sound {sound}");
            return null;
        }

        private void Awake()
        {
            Debug.Assert(audioSource != null, "audioSource cannot be bull");
            Debug.Assert(soundClips != null && soundClips.Length != 0, "sound clip need to be set up");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        }
    }
}
