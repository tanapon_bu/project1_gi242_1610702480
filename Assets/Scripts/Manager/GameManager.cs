﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PlayerShip;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI Score;
    public TextMeshProUGUI Hp;


    public GameObject Boss;
    public GameObject BackGroundLV1;
    public GameObject BackGroundLV2;
    public GameObject Win;
    public GameObject Lose;
    public GameObject Player;
    public static bool isLevel2;
    public static bool isBossAlive;


    // Start is called before the first frame update
    void Start()
    {
        isLevel2 = false;
        isBossAlive = true;

        SoundManager.Instance.PlayBGM();
        Hp = GameObject.Find("Canvas/hpText").GetComponent<TextMeshProUGUI>();
        Score = GameObject.Find("Canvas/ScoreText").GetComponent<TextMeshProUGUI>();


    }

    // Update is called once per frame
    void Update()
    {
        Hp.text = "HP : " + PlayerController.health;
        Score.text = "Score : " + PlayerController.playerScore;

        SetActive();
        if (Time.timeScale == 0)
        {
            Destroy(this.gameObject);
        }
    }

    void SetActive()
    {
        if (PlayerController.playerScore >= 50 )
        {
            BackGroundLV1.SetActive(false);
            Boss.SetActive(true);
            BackGroundLV2.SetActive(true);
            isLevel2 = true;
        }
        else
        {
            isLevel2 = false;
            BackGroundLV1.SetActive(true);
            Boss.SetActive(false);
            BackGroundLV2.SetActive(false);
        }

        if(isLevel2 == true)
        {
            if (isBossAlive != true)
            {
                Boss.SetActive(false);
                Win.SetActive(true);
                Player.SetActive(false);
                Time.timeScale = 0;
            }
            else
            {
                Player.SetActive(true);
                Win.SetActive(false);
                Time.timeScale = 1;
            }
        }

        if (PlayerController.health <= 0)
        {
            Time.timeScale = 0;
            Lose.SetActive(true);
        }
        else
        {
            Lose.SetActive(false);
        }


    }
}
