﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerShip;

    public class EnemyScript : MonoBehaviour
    {
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] AudioClip enemyFireSound;
        [SerializeField] float enemyFireSoundVolume = 0.1f;
        [SerializeField] AudioClip enemyDied;
        [SerializeField] float enemyDiedSoundVolume = 0.1f;

        private int health = 100;
        private float speed = 2f;

        public static int damage;


        // Start is called before the first frame update
        void Start()
        {
            damage = 5;
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);

            if (PlayerController.playerScore >= 50)
            {
                Destroy(this.gameObject);
            }
            if (PlayerController.health <= 0)
            {
                Destroy(this.gameObject);
            }
        if (Time.timeScale == 0)
        {
            Destroy(this.gameObject);
        }
    }

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.tag == "Bullet")
            {
                health -= Bullet.DamageNormalBullet;

                if (health <= 0)
                {
                    PlayerController.playerScore += 10;
                    AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
                    Destroy(this.gameObject);
                }

            }

            if (col.gameObject.tag == "DestroyWall")
            {
                Destroy(this.gameObject);
            }
        }
    }
