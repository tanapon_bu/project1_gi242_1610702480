﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerShip;
using Manager;


public class BossScript : MonoBehaviour
{
    public static int bossHealth;
    public static int bossDmg;

    public GameObject BossBullet;
    public Transform BulletPosMid;
    public Transform BulletPosLeft;
    public Transform BulletPosRight;

    private float timeerSpeed = 2f;
    private float elapsed;

    [SerializeField] protected AudioSource audioSource;
    [SerializeField] AudioClip enemyFireSound;
    [SerializeField] float enemyFireSoundVolume = 0.1f;
    [SerializeField] AudioClip enemyDied;
    [SerializeField] float enemyDiedSoundVolume = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        bossHealth = 200;
        bossDmg = 15;
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if(elapsed >= timeerSpeed)
        {
            elapsed = 0f;
            BulletTime();

            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
        }
    }
    void BulletTime()
    {
        Instantiate(BossBullet, BulletPosMid.position, BulletPosMid.rotation);
        Instantiate(BossBullet, BulletPosLeft.position, BulletPosLeft.rotation);
        Instantiate(BossBullet, BulletPosRight.position, BulletPosRight.rotation);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            bossHealth -= Bullet.DamageNormalBullet;

            if (bossHealth <= 0)
            {
                GameManager.isBossAlive = false;
                PlayerController.playerScore += 50;
                AudioSource.PlayClipAtPoint(enemyDied, Camera.main.transform.position, enemyDiedSoundVolume);
            }
        }
        if (col.gameObject.tag == "DestroyWall")
        {
            Destroy(this.gameObject);
        }
    }
}
