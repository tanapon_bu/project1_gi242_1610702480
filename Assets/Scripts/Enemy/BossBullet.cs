﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerShip;

public class BossBullet : MonoBehaviour
{
    public float speed = 1f;
    public Rigidbody2D rb;

    public static int DamageNormalBullet = 20;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity =  Vector3.zero * speed;
    }
    private void Update()
    {
        if (Time.timeScale == 0)
        {
            Destroy(this.gameObject);
        }
    }
    // Update is called once per frame

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }

        if (col.gameObject.tag == "DestroyWall")
        {
            Destroy(this.gameObject);
        }
        if (col.gameObject.tag == "Bullet")
        {
            Destroy(this.gameObject);
        }

    }
}
